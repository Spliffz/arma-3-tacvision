/*
	TacVision 0.01a (Tactical Vision)
	
	Provides the user with a Tactical overview/display/hud when wearing tactical glasses/shades.
	
	Author: Spliffz <theSpliffz@gmail.com>
*/
if(isDedicated) exitWith {};

waitUntil { player == player };

#include "tacVision_defines.sqf";

if (!TacVision_Enabled) exitWith {};

disableSerialization;


// important includes
#include "tacVision_functions.sqf";



// dynamic hud stuff
_draw3D = addMissionEventHandler ["Draw3D", { _this call TacVision_MainLoop; }];



// static hud stuff
while {TacVision_Enabled} do {
	private ["_goggle", "_ui", "_hud"];
	_goggle = goggles player;
	if(_goggle in TacVision_goggles) then {
		hint format ["goggles: %1", _goggle];
		
		17201 cutRsc ["tacVision", "PLAIN", 1, false];
		//spliffz_TV_tacVisionDialog = createDialog "tacVision";
		//tacVision_currentCompassHeading ctrlSetFade 0;
		//ctrlEnable [17110, true];

		_ui = uiNamespace getVariable "tacVision_HUD";
		
		// compass
		_hud = _ui displayCtrl 17110;
		_hud ctrlSetFade 0;
		_hud ctrlCommit 0;
		[_ui, _hud] spawn TacVision_getCurrentCompassHeading;
		
		// wind bearing
		_hud = _ui displayCtrl 17111;
		_hud ctrlSetFade 0;
		_hud ctrlCommit 0;
		[_ui, _hud] spawn TacVision_getCurrentWindDirection;
		
		// wind strenght
		_hud = _ui displayCtrl 17112;
		_hud ctrlSetFade 0;
		_hud ctrlCommit 0;
		[_ui, _hud] spawn TacVision_getCurrentWindStrength;
		
		
/*
		_hud = _ui displayCtrl 17300;
		_hud ctrlSetFade 0;
		_hud ctrlCommit 0;
		_hud = _ui displayCtrl 17301;
		_hud ctrlSetFade 0;
		_hud ctrlCommit 0;
*/


		[] call TacVision_TI_Scanner;
		
		
		TacVision_Enabled = false;
	};
	
};


/* wtf.. ??
TacVision_Action_Shades = {
	_mode = _this select 0;
	if(_mode == 1) then {
		private ["_ui", "_shade", "_shade"];
		_ui = uiNamespace getVariable "tacVision_HUD";
		_shade = _ui displayCtrl 17210; // <-- mist een ; ofzo
		_shade ctrlSetFade 0;
		_shade ctrlCommit 0;
		removeAction TacVision_ShadeAction;
		TacVision_ShadeAction = player addAction ["Shades: Off", " [0] call TacVision_Action_Shades; "];
	} else {
		private ["_ui", "_shade", "_shade"];
		_ui = uiNameSpace getVariable "tacVision_HUD";
		_shade = _ui displayCtrl 17210;
		_shade ctrlSetFade 1;
		_shade ctrlCommit 0;
		removeAction TacVision_ShadeAction;
		TacVision_ShadeAction = player addAction ["Shades: Red", " [1] call TacVision_Action_Shades; "];
	};
};

TacVision_ShadeAction = player addAction ["Shades: Red", " [1] call TacVision_Action_Shades; "];
*/








// EOF