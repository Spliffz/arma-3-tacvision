/*
	TacVision 0.01a (Tactical Vision)
*/

//TacVision_MainLoop = {
	private ["_goggle"];
	_goggle = goggles player;
	onEachFrame {
		if(_goggle in TacVision_goggles) then {
			private ["_pos", "_i", "_intel", "_vPos", "_target", "_mark", "_size", "_tSide", "_Newpos", "_tDistance"];
			// put intel into draw3d thingie and display
			diag_log format ["## TacVision Debug ## - missionEventHandler: TacVision_ETI_Intel: %1", TacVision_ETI_Intel];
			if(count TacVision_ETI_Intel > 0) then {
				for "_i" from 0 to (count (TacVision_ETI_Intel)-1) do {
					_intel = TacVision_ETI_Intel select _i;
					_pos = _intel select 0;
					_target = _intel select 1;
					_mark = _intel select 2;
					_size = _intel select 3;
					_tSide = _intel select 4;
					_vPos = _intel select 5;
					_Newpos = _vPos;
					_Newpos set[2,(getPosATL _target select 2) + 1.2];

					_tDistance = _intel select 6;
					//[_mark, TACVISION_HUDCOLOR, [_vPos select 0, _vPos select 1, 1.3], _size, _size, 0, "", 0] call TacVision_TI_draw3dIcon;
					[_mark, TACVISION_HUDCOLOR, _Newpos, _size, _size, 0, "", 0] call TacVision_TI_draw3dIcon;
				};
				
			};
			
			//[texture, color, pos, width, height, angle, text, shadow, textSize, font]: Array
			//[MISSION_ROOT+"TacVision\markers\marker_eti_target.paa", [0,0.97,0.87,1], [_pos select 0, _pos select 1, 1.3], 5, 5, 0, "", 1] call TacVision_TI_draw3dIcon;
			//["", [0,0,0,1], [_pos select 0, _pos select 1, 1.3], 0, 0, 0, (name player), 1] call TacVision_TI_draw3dIcon;
		};
	};
//};


// EOF