/*
	TacVision 0.01a (Tactical Vision)
*/

// scans for persons/vehicles
//TacVision_TI_Scanner = {
	private ["_targets"];
	disableSerialization;
	//_scanTypes = ["Man", "ManDiver", "ManStory", "Static", "Air", "Autonomous", "Car", "Armored", "Support", "Ship", "SubMmarine"];
	
	while {true} do {
	
		/*
		call TacVision_GarbageCollector;
		waitUntil { TacVision_GarbageCollector };
		*/
		private ["_del", "_i", "_item", "_pos"];
		_del = [];
		for "_i" from 0 to (count (TacVision_ETI_Intel)-1) do {
			_item = TacVision_ETI_Intel select _i;
			_pos = _item select 0;
			if((_pos distance player) > 100) then {
				_del set [(count _del), _i];
			};
		};
		//diag_log format ["## TacVision Debug ## - TacVision_GarbageCollector: _del: %1", _del];
		if(count _del > 0) then {
			TacVision_ETI_Intel = [TacVision_ETI_Intel, _del] call BIS_fnc_removeIndex;
		};
		//diag_log format ["## TacVision Debug ## - TacVision_GarbageCollector: TacVision_ETI_Intel: %1", TacVision_ETI_Intel];

		
		_targets = nearestObjects [player, TACVISION_TI_SCANNER_OBJECTLIST, TACVISION_TI_SCANNER_MAXRANGE];
		//diag_log format ["## TacVision Debug ## - _targets: %1", _targets];
		{
			
			if( _x isKindOf "Man" || _x isKindOf "ManStory" || _x isKindOf "ManDiver" || _x isKindOf "ManUrban" || _x isKindOf "ManSniper" || _x isKindOf "ManRecon" || _x isKindOf "ManSupport" && (_x distance player) < TACVISION_TI_SCANNER_INFANTRY_RANGE) then {
				//diag_log format ["## TacVision Debug ## - _x: %1", _x];
				private ["_intel"];
				_type = "Man";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_INFANTRY, TACVISION_MARKER_SIZE_MEDIUMSMALL, _type] call TacVision_TI_ETI;
				_intel = nil;
			};

			if(_x isKindOf "Car" || _x isKindOf "Support" && (_x distance player) < TACVISION_TI_SCANNER_VEHICLE_RANGE) then {
				private ["_intel"];
				_type = "Car";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_VEHICLE, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			if(_x isKindOf "Air" || _x isKindOf "Autonomous" && (_x distance player) < TACVISION_TI_SCANNER_AIR_RANGE) then {
				private ["_intel"];
				_type = "Air";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_AIR, TACVISION_MARKER_SIZE_LARGE, _type] call TacVision_TI_ETI;
				_intel = nil;
			};

			if(_x isKindOf "Armored" && (_x distance player) < TACVISION_TI_SCANNER_ARMORED_RANGE) then {
				private ["_intel"];
				_type = "Armored";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_ARMORED, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			if(_x isKindOf "Ship" && (_x distance player) < TACVISION_TI_SCANNER_SHIP_RANGE) then {
				private ["_intel"];
				_type = "Ship";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_SHIP, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			if(_x isKindOf "SubMarine" && (_x distance player) < TACVISION_TI_SCANNER_SUBMARINE_RANGE) then {
				private ["_intel"];
				_type = "SubMarine";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_SUBMARINE, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			/*
			if(_x isKindOf "Static") then {
				private ["_intel"];
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_STATIC, TACVISION_MARKER_SIZE_MEDIUMSMALL] call TacVision_TI_ETI;
				_intel = nil;
			};
			*/
		} forEach _targets;
	};
//};


// EOF