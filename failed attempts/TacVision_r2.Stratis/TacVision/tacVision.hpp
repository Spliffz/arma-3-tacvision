/*
	TacVision 0.01a (Tactical Vision)
*/
//class RscTitles
//{
	
	class tacVision
	{
		
		idd = 17100;
		duration = 1.0e+11; // -1 & 0 not unlimited 999999999999999999
		movingEnable = 0;
		enableSimulation = 1;
		name = "tacVision_HUD";
		onLoad = "uiNamespace setVariable ['tacVision_HUD', _this select 0]";
		controlsBackground[] = {};
		objects[] = {};

		class controls
		{
			class tacVision_currentCompassHeading
			{
				
				idc = 17110;
				type = 0;
				style = 2;
				x = safeZoneX + safeZoneW - 0.1 * 3 / 4;
				y = safeZoneY + safeZoneH - 0.1;
				h = 0.1;
				w = 0.1 * 3 / 4; // w == h
				font="PuristaMedium";
				sizeEx = 0.03;
				colorBackground[] = {1,1,0,1};
				colorText[] = {0,0,1,1};
				text = "32";
			};

			class tacVision_currentWindDirection
			{
				
				idc = 17111;
				type = 0;
				style = 2;
				x = safeZoneX + safeZoneW - 0.1 * 3 / 4;
				y = safeZoneY + safeZoneH - 0.2;
				h = 0.1;
				w = 0.1 * 3 / 4; // w == h
				font="PuristaMedium";
				sizeEx = 0.03;
				colorBackground[] = {1,1,0,1};
				colorText[] = {0,0,1,1};
				text = "180";
			};

			class tacVision_currentWindSpeed
			{
				
				idc = 17112;
				type = 0;
				style = 2;
				x = safeZoneX + safeZoneW - 0.1 * 3 / 4;
				y = safeZoneY + safeZoneH - 0.3;
				h = 0.1;
				w = 0.1 * 3 / 4; // w == h
				font="PuristaMedium";
				sizeEx = 0.03;
				colorBackground[] = {1,1,0,1};
				colorText[] = {0,0,1,1};
				text = "230";
			};


			/*
			// sunglasses shade colors
			class tacVision_shade_red
			{
				
				idc = 17210;
				type = 0;
				style = 2;
				x = safeZoneX + safeZoneW - 0.1 * 3 / 4;
				y = safeZoneY + safeZoneH - 0.3;
				h = 1;
				w == 1;
				font="PuristaMedium";
				sizeEx = 0.03;
				colorBackground[] = {1,0,0,0.3};
				colorText[] = {0,0,1,1};
				text = "";
			};
			*/
			
			/*
			// Upper Info Windows
			class TacVision_targetInfoWindow // : RscStructuredText
			{
				idc = 17300;
				type = 13;
				style = 2;
				x = 0.242187 * safezoneW + safezoneX;
				y = -0.00599999 * safezoneH + safezoneY;
				w = 0.149531 * safezoneW;
				h = 0.11 * safezoneH;
				font="PuristaMedium";
				sizeEx = 0.03;
				colorBackground[] = {1,1,0,1};
				colorText[] = {0,0,1,1};
				text = "";
			};
			class TacVision_navInfoWIndow // : RscStructuredText
			{
				idc = 17301;
				type = 13;
				style = 2;
				x = 0.628906 * safezoneW + safezoneX;
				y = -0.00599999 * safezoneH + safezoneY;
				w = 0.12375 * safezoneW;
				h = 0.055 * safezoneH;
				font="PuristaMedium";
				sizeEx = 0.03;
				colorBackground[] = {1,1,0,1};
				colorText[] = {0,0,1,1};
				text = "";
			};
			class TacVision_targetInfoWindowTextOverlay // : RscStructuredText
			{
				idc = 17302;
				type = 13;
				style = 2;
				text = "text_op_deze"; //--- ToDo: Localize;
				x = 0.242187 * safezoneW + safezoneX;
				y = -0.00599999 * safezoneH + safezoneY;
				w = 0.149531 * safezoneW;
				h = 0.11 * safezoneH;
				font="PuristaMedium";
				sizeEx = 0.03;
				colorBackground[] = {1,1,0,1};
				colorText[] = {0,0,1,1};
			};
			class TacVision_navInfoWIndowTextOverlay // : RscStructuredText
			{
				idc = 17303;
				type = 13;
				style = 2;
				text = "Text Op Deze"; //--- ToDo: Localize;
				x = 0.628906 * safezoneW + safezoneX;
				y = -0.00599999 * safezoneH + safezoneY;
				w = 0.12375 * safezoneW;
				h = 0.055 * safezoneH;
				font="PuristaMedium";
				sizeEx = 0.03;
				colorBackground[] = {1,1,0,1};
				colorText[] = {0,0,1,1};
			};
*/

		};
		
	};
	
//};
