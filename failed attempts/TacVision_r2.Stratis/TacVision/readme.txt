TacVision 0.01a (Tactical Vision)

Provides the user with a Tactical overview/display/hud when wearing tactical glasses/shades.

Author: Spliffz <theSpliffz@gmail.com>

Displays:
- current heading
- wind dir/strength
- squad info?
- current time, get from map?

- color-tint overlay for shades?

- Thermal vision?
- FS NVG

- ATR - Automatic Target Recognition
- IFF/Target Identification w/ Extended Target Information
	- Patrol/Combat mode

- player health - sidebar

- divide features by rank? from soldier (private) to specops (sergeant or higher)
	
		
When in chopper as gunner:
	- hud as in pilot hud. 
	- ATR w/ ETI
		
#################################
#################################

	IMPORTANTA!!
	moving objects give lag in hud display.

#################################
#################################




------------------------------------------------------------------------------

missioneventhandler = draait hele missie lang, kortom: main check? > call functie waarin alles gedaan word, info teruggeeft voor display


------------------------------------------------------------------------------
player put glasses on (in inventory) > player setVariable ["TacVision_Enabled",true];


// check if wearing glasses
TacVision_Enabled = player getVariable "TacVision_Enabled";

// world stuff
addMissionEventHandler ["Draw3D", {
	if(TacVision_Enabled) do {
	
	};
}];


// static hud stuff
while (TacVision_Enabled) do {
	
};





"## TacVision Debug ## - TacVision_TI_ETI: _target: B Alpha 1-1:1 (Spliffz), _tType: , _tSide: CIV, _tDistance: 0, _tPos: [1894.77,5759.43,0.00143862], _tVisiblePos: [1894.77,5759.43,0.00143862]"
"## TacVision Debug ## - TacVision_TI_ETI: _target: B Alpha 1-2:1, _tType: , _tSide: WEST, _tDistance: 4.69904, _tPos: [1891.94,5763.17,0.00143862], _tVisiblePos: [1891.94,5763.17,0.00143862]"
"## TacVision Debug ## - TacVision_TI_ETI: _target: O Alpha 1-1:1, _tType: , _tSide: EAST, _tDistance: 5.02085, _tPos: [1890.06,5761.16,0.00143862], _tVisiblePos: [1890.06,5761.16,0.00143862]"



"## TacVision Debug ## - missionEventHandler: 
TacVision_ETI_Intel: [
[[1891.93,5763.17,0.00143862],B Alpha 1-2:1,CIV,[1891.93,5763.17,0.00143862],11.6904],
[[1893.19,5765.85,0.00143862],O Alpha 1-1:1,CIV,[1893.19,5765.85,0.00143862],13.2167],
[[1937.47,5783.82,0.00838089],Agent 0x3cf32080,CIV,[1937.56,5783.79,0.00837994],48.2025],
[[1887.15,5802.82,0.0083952],Agent 0x274cc080,CIV,[1887.06,5802.88,0.00839376],50.143],
[[1950.91,5766.6,0.00839901],Agent 0x3bc0e080,CIV,[1950.8,5766.59,0.00839996],52.9462]
]"




// IMPORTANT! TacVision GUI Editor Stuff Source!

/* #Roqini
$[
	1.063,
	["TacVision",[[0,0,1,1],0.025,0.04,"GUI_GRID"],0,0,0],
	[1100,"TacVision_targetInfoWindow",[1,"",["0.242187 * safezoneW + safezoneX","-0.00599999 * safezoneH + safezoneY","0.149531 * safezoneW","0.11 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1101,"TacVision_navInfoWIndow",[1,"",["0.628906 * safezoneW + safezoneX","-0.00599999 * safezoneH + safezoneY","0.12375 * safezoneW","0.055 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1102,"TacVision_targetInfoWindowTextOverlay",[1,"text_op_deze",["0.242187 * safezoneW + safezoneX","-0.00599999 * safezoneH + safezoneY","0.149531 * safezoneW","0.11 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1103,"TacVision_navInfoWIndowTextOverlay",[1,"Text Op Deze",["0.628906 * safezoneW + safezoneX","-0.00599999 * safezoneH + safezoneY","0.12375 * safezoneW","0.055 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]]
]
*/










