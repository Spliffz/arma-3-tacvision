
#include "config\tacVision_defines.sqf"

disableSerialization;

[player] spawn {

	disableSerialization;

	/*
		#################
		## This currently results in a 20-40 FPS drop!! - wait what? need to fix that shit.
		## it works though..
		#################
	*/
	onEachFrame {
		//diag_log format ["## TACVISION DEBUG ## - onEachFrame: Entered! Calling TacVision_OnEachFrame ..."];
		//_this call TacVision_OnEachFrame;

		//_targets = nearestObjects [position player, TACVISION_TI_SCANNER_OBJECTLIST, TACVISION_TI_SCANNER_MAXRANGE];
		//_targets = nearestObjects [position player, ["Man"], 200];
		_targets = position player nearObjects ["Man", 200];
		//diag_log format ["## TacVision Debug ## - _targets: %1", _targets];
		{
			
			// filter targets
			
			// get target intel
			//_ret = [_tPos, _target, _tMarker, _tSize, _tside, _tVisiblePos, _tDistance, _speed, _flyingheight];

			// create marker
			
			_typeMen = ["Man", "ManStory", "ManDiver", "ManUrban", "ManSniper", "ManRecon", "ManSupport"];
			
			if((_x isKindOf "Man") || (_x isKindOf "ManStory") || (_x isKindOf "ManDiver") || (_x isKindOf "ManUrban") || (_x isKindOf "ManSniper") || (_x isKindOf "ManRecon") || (_x isKindOf "ManSupport") && ((_x distance player) < TACVISION_TI_SCANNER_INFANTRY_RANGE) && (alive _x)) then {
				//diag_log format ["## TacVision Debug ## - _x: %1", _x];
				private ["_intel"];
				_type = "Man";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_INFANTRY, TACVISION_MARKER_SIZE_MEDIUMSMALL, _type] call TacVision_TI_ETI;
				//diag_log format ["## TacVision Debug ## - _intel: %1", _intel];
				
				[_intel, TACVISION_HUDCOLOR] call TacVision_TI_MarkIt;
			};
			

			/*
			
			if(_x isKindOf "Car" || _x isKindOf "Support" && (_x distance player) < TACVISION_TI_SCANNER_VEHICLE_RANGE) then {
				private ["_intel"];
				_type = "Car";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_VEHICLE, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			
			
			if(_x isKindOf "Air" || _x isKindOf "Autonomous" && (_x distance player) < TACVISION_TI_SCANNER_AIR_RANGE) then {
				private ["_intel"];
				_type = "Air";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_AIR, TACVISION_MARKER_SIZE_LARGE, _type] call TacVision_TI_ETI;
				_intel = nil;
			};

			if(_x isKindOf "Armored" && (_x distance player) < TACVISION_TI_SCANNER_ARMORED_RANGE) then {
				private ["_intel"];
				_type = "Armored";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_ARMORED, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			if(_x isKindOf "Ship" && (_x distance player) < TACVISION_TI_SCANNER_SHIP_RANGE) then {
				private ["_intel"];
				_type = "Ship";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_SHIP, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			if(_x isKindOf "SubMarine" && (_x distance player) < TACVISION_TI_SCANNER_SUBMARINE_RANGE) then {
				private ["_intel"];
				_type = "SubMarine";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_SUBMARINE, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			/*
			if(_x isKindOf "Static") then {
				private ["_intel"];
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_STATIC, TACVISION_MARKER_SIZE_MEDIUMSMALL] call TacVision_TI_ETI;
				_intel = nil;
			};
			*/
		} forEach _targets;

	}; // end oneachframe
	
};



// static hud stuff
while {TacVision_Enabled} do {
	private ["_goggle", "_ui", "_hud"];
	_goggle = goggles player;
	if(_goggle in TacVision_goggles) then {
		hint format ["goggles: %1", _goggle];
		
		17201 cutRsc ["tacVision", "PLAIN", 1, false];
		//spliffz_TV_tacVisionDialog = createDialog "tacVision";
		//tacVision_currentCompassHeading ctrlSetFade 0;
		//ctrlEnable [17110, true];

		_ui = uiNamespace getVariable "tacVision_HUD";
		
		// compass
		_hud = _ui displayCtrl 17110;
		_hud ctrlSetFade 0;
		_hud ctrlCommit 0;
		[_ui, _hud] spawn TacVision_getCurrentCompassHeading;
		
		// wind bearing
		_hud = _ui displayCtrl 17111;
		_hud ctrlSetFade 0;
		_hud ctrlCommit 0;
		[_ui, _hud] spawn TacVision_getCurrentWindDirection;
		
		// wind strenght
		_hud = _ui displayCtrl 17112;
		_hud ctrlSetFade 0;
		_hud ctrlCommit 0;
		[_ui, _hud] spawn TacVision_getCurrentWindStrength;
		
		
/*
		_hud = _ui displayCtrl 17300;
		_hud ctrlSetFade 0;
		_hud ctrlCommit 0;
		_hud = _ui displayCtrl 17301;
		_hud ctrlSetFade 0;
		_hud ctrlCommit 0;
*/


		//[] call TacVision_TI_Scanner;
		
		
		TacVision_Enabled = false;
	};
	
};


