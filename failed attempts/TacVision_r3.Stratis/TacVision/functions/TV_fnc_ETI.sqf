/*
	TacVision 0.01a (Tactical Vision)
*/

// Extended Target Information [ETI]
//TacVision_TI_ETI = {
	private ["_this", "_target", "_tType", "_tSide", "_tDistance", "_tPos", "_tMarker"];
	disableSerialization;
	_target = _this select 0;
	if(_target == player) exitWith {}; // don't when player
	_isAgent = ["Agent ", str _target] call BIS_fnc_inString; 
	if(_isAgent) exitWith {}; // don't when animal/ambience
	_tMarker = _this select 1;
	_tSize = _this select 2;
	_tType = _this select 3; 
	_tSide = side _target;
	_tDistance = _target distance player;
	_tPos = getPosATL _target;
	_tVisiblePos = visiblePosition _target;

	//diag_log format ["## TacVision Debug ## - TacVision_TI_ETI: _target: %1, _tType: %2, _tSide: %3, _tDistance: %4, _tPos: %5, _tVisiblePos: %6", _target, _tType, _tSide, _tDistance, _tPos, _tVisiblePos];
	
	// if vehicle
	_speed = 0;
//	if!(_target isKindOf "Man") then { // || _target isKindOf "Static") then {
	if(_type != "Man") then {
		_tVisiblePos = _tPos;
		_speed = speed _target;
	} else { 
		_speed = 0;
	};
	
	// if air
	_flyingheight = 0;
//	if(_target isKindOf "Air" || _target isKindOf "Autonomous") then {
	if(_type == "Air" || _type == "Autonomous") then {
		_pasl = getPosASL _target;
		_patl = getPosATL _target;
		_h1 = _pasl select 2;
		_h2 = _patl select 2;
		_flyingheight = _h1 - _h2;
	} else {
		_flyingheight = 0;
	};

	// check hostility
	
	// if target moves behind cover/out of LOS, loose the marker
	
	// intel array
	_ret = [_tPos, _target, _tMarker, _tSize, _tside, _tVisiblePos, _tDistance, _speed, _flyingheight];
	diag_log format ["## TacVision Debug ## - TacVision_TI_ETI: _ret: %1", _ret];
	TacVision_ETI_Intel set [count (TacVision_ETI_Intel), _ret];
	
	//["marker_eti_target.paa", [], [], 5, 5, 0, "", 0]]

//};

// EOF