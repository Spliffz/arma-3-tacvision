/*
	TacVision 0.01a (Tactical Vision)
*/


TacVision_GarbageCollector = {
	private ["_del", "_i", "_item", "_pos"];
	_del = [];
	for "_i" from 0 to (count (TacVision_ETI_Intel)-1) do {
		_item = TacVision_ETI_Intel select _i;
		_pos = _item select 0;
		if((_pos distance player) > 100) then {
			_del set [(count _del), _i];
		};
	};
	diag_log format ["## TacVision Debug ## - TacVision_GarbageCollector: _del: %1", _del];
	TacVision_ETI_Intel = [TacVision_ETI_Intel, _del] call BIS_fnc_removeIndex;
	diag_log format ["## TacVision Debug ## - TacVision_GarbageCollector: TacVision_ETI_Intel: %1", TacVision_ETI_Intel];
	
	true
};


TacVision_CompassDegreesToString = {
	_degree = _this select 0;
	_str = "";
	if(_degree >= 350 && _degree <= 10) then { _str = "N"; };
	if(_degree >= 11 && _degree <= 79) then { _str = "NE"; };
	if(_degree >= 80 && _degree <= 100) then { _str = "E"; };
	if(_degree >= 101 && _degree <= 169) then { _str = "SE"; };
	if(_degree >= 170 && _degree <= 190) then { _str = "S"; };
	if(_degree >= 191 && _degree <= 259) then { _str = "SW"; };
	if(_degree >= 260 && _degree <= 279) then { _str = "W"; };
	if(_degree >= 280 && _degree <= 349) then { _str = "NW"; };
	
	_str
};


/*
onEachFrame
{
    private["_vis","_pos"];
    {
        if(player distance _x < 500 && side _x == west && alive _x && _x != player) then
        {
                _pos = visiblePosition _x;
                _pos set[2,(getPosATL _x select 2) + 2.2];
		_color = [0,0,0.64,1];
                drawIcon3D ["\a3\ui_f\data\map\Markers\NATO\b_inf.paa",_color,_pos,1,1,0,name _x,0,0.04];
        };
    } foreach allUnits;
};


TacVision_MainLoop = {
	private ["_goggle"];
	_goggle = goggles player;
	if(_goggle in TacVision_goggles) then {
		private ["_pos", "_i", "_intel", "_vPos", "_target", "_mark", "_size", "_tSide", "_Newpos", "_tDistance"];
		// put intel into draw3d thingie and display
		diag_log format ["## TacVision Debug ## - missionEventHandler: TacVision_ETI_Intel: %1", TacVision_ETI_Intel];
		if(count TacVision_ETI_Intel > 0) then {
			for "_i" from 0 to (count (TacVision_ETI_Intel)-1) do {
				_intel = TacVision_ETI_Intel select _i;
				_pos = _intel select 0;
				_target = _intel select 1;
				_mark = _intel select 2;
				_size = _intel select 3;
				_tSide = _intel select 4;
				_vPos = _intel select 5;
                _Newpos = _vPos;
                _Newpos set[2,(getPosATL _target select 2) + 1.2];

				_tDistance = _intel select 6;
				//[_mark, TACVISION_HUDCOLOR, [_vPos select 0, _vPos select 1, 1.3], _size, _size, 0, "", 0] call TacVision_TI_draw3dIcon;
				[_mark, TACVISION_HUDCOLOR, _Newpos, _size, _size, 0, "", 0] call TacVision_TI_draw3dIcon;
			};
			
		};
		
		//[texture, color, pos, width, height, angle, text, shadow, textSize, font]: Array
		//[MISSION_ROOT+"TacVision\markers\marker_eti_target.paa", [0,0.97,0.87,1], [_pos select 0, _pos select 1, 1.3], 5, 5, 0, "", 1] call TacVision_TI_draw3dIcon;
		//["", [0,0,0,1], [_pos select 0, _pos select 1, 1.3], 0, 0, 0, (name player), 1] call TacVision_TI_draw3dIcon;
	};
};
*/

/*
//["",[0,0,0,1],_pos,0,0,0,_text,0] call TacVision_TI_draw3dIcon;
TacVision_TI_draw3dIcon = {
	private["_count", "_icon", "_color", "_pos", "_sizeX", "_sizeY", "_dir", "_text", "_shadow", "_sizeT", "_font"];

	//diag_log format ["## TacVision Debug ## - TacVision_TI_draw3dIcon: _this:  %1", _this];

	_count = count _this;
	_icon = if(_count > 0) then { _this select 0 } else { "" };
	_color	= if(_count > 1) then {_this select 1} else {[0,0,0,1]}; // black
	_pos = if(_count > 2) then {_this select 2} else {[0,0,0]};
	_sizeX = if(_count > 3) then { _this select 3 } else { 1 };
	_sizeY = if(_count > 4) then { _this select 4 } else { 1 };
	_dir = if(_count > 5) then { _this select 5 } else { 0 };
	_text = if(_count > 6) then { _this select 6 } else { "" };
	_shadow = if(_count > 7) then { _this select 7 } else { 1 };
	
	//diag_log format ["## TacVision Debug ## - TacVision_TI_draw3dIcon: %1, %2, %3, %4, %5, %6, %7, %8", _icon, _color, _pos, _sizeX, _sizeY, _dir, _text, _shadow];

	drawIcon3D [
		_icon,
		_color,
		_pos,
		_sizeX,
		_sizeY,
		_dir,
		_text,
		_shadow,
		0.04,
		"PuristaLight"
	];

};
*/

// scans for mines
TacVision_TI_OrdnanceDetector = {
	private [];
	disableSerialization;
};


/*
TacVision_TI_MarkIt = {
	private ["_intel"];
	_intel = _this select 0;
	if(typeName _intel == "ARRAY") then {
		private ["_t_pos", "_t_target", "_t_mark", "_t_size", "_t_tSide", "_t_vPos", "_t_NewPos", "_t_tDistance"];
		// create marker
		_t_pos = _intel select 0;
		_t_target = _intel select 1;
		_t_mark = _intel select 2;
		_t_size = _intel select 3;
		_t_tSide = _intel select 4;
		_t_vPos = _intel select 5;
		_t_Newpos = _t_vPos;
		_t_Newpos set[2,(getPosATL _t_target select 2) + 1.2];

		_t_tDistance = _intel select 6;
		//[_mark, TACVISION_HUDCOLOR, [_vPos select 0, _vPos select 1, 1.3], _size, _size, 0, "", 0] call TacVision_TI_draw3dIcon;
		[_t_mark, TACVISION_HUDCOLOR, _t_Newpos, _t_size, _t_size, 0, "", 0] call TacVision_TI_draw3dIcon;
		
		_intel = nil;
	};
};
*/

/*
// Extended Target Information [ETI]
TacVision_TI_ETI = {
	private ["_this", "_target", "_tType", "_tSide", "_tDistance", "_tPos", "_tMarker"];
	disableSerialization;
	_target = _this select 0;
	if(_target == player) exitWith {}; // don't when player
	_isAgent = ["Agent ", str _target] call BIS_fnc_inString; 
	if(_isAgent) exitWith {}; // don't when animal/ambience
	_tMarker = _this select 1;
	_tSize = _this select 2;
	_tType = _this select 3; 
	_tSide = side _target;
	_tDistance = _target distance player;
	_tPos = getPosATL _target;
	_tVisiblePos = visiblePosition _target;

	//diag_log format ["## TacVision Debug ## - TacVision_TI_ETI: _target: %1, _tType: %2, _tSide: %3, _tDistance: %4, _tPos: %5, _tVisiblePos: %6", _target, _tType, _tSide, _tDistance, _tPos, _tVisiblePos];
	
	// if vehicle
	_speed = 0;
//	if!(_target isKindOf "Man") then { // || _target isKindOf "Static") then {
	if(_type != "Man") then {
		_tVisiblePos = _tPos;
		_speed = speed _target;
	} else { 
		_speed = 0;
	};
	
	// if air
	_flyingheight = 0;
//	if(_target isKindOf "Air" || _target isKindOf "Autonomous") then {
	if(_type == "Air" || _type == "Autonomous") then {
		_pasl = getPosASL _target;
		_patl = getPosATL _target;
		_h1 = _pasl select 2;
		_h2 = _patl select 2;
		_flyingheight = _h1 - _h2;
	} else {
		_flyingheight = 0;
	};

	// check hostility
	
	// if target moves behind cover/out of LOS, loose the marker
	
	// intel array
	_ret = [_tPos, _target, _tMarker, _tSize, _tside, _tVisiblePos, _tDistance, _speed, _flyingheight];
	//diag_log format ["## TacVision Debug ## - TacVision_TI_ETI: _ret: %1", _ret];
	//TacVision_ETI_Intel set [count (TacVision_ETI_Intel), _ret];
	
	_ret
	
	//["marker_eti_target.paa", [], [], 5, 5, 0, "", 0]]
};
*/

/*

// scans for persons/vehicles
TacVision_TI_Scanner = {
	private ["_targets"];
	disableSerialization;
	//_scanTypes = ["Man", "ManDiver", "ManStory", "Static", "Air", "Autonomous", "Car", "Armored", "Support", "Ship", "SubMmarine"];
	
	while {true} do {
	
		/*
		call TacVision_GarbageCollector;
		waitUntil { TacVision_GarbageCollector };
		*/

/*
		private ["_del", "_i", "_item", "_pos"];
		_del = [];
		for "_i" from 0 to (count (TacVision_ETI_Intel)-1) do {
			_item = TacVision_ETI_Intel select _i;
			_pos = _item select 0;
			if((_pos distance player) > 100) then {
				_del set [(count _del), _i];
			};
		};
		//diag_log format ["## TacVision Debug ## - TacVision_GarbageCollector: _del: %1", _del];
		if(count _del > 0) then {
			TacVision_ETI_Intel = [TacVision_ETI_Intel, _del] call BIS_fnc_removeIndex;
		};
		//diag_log format ["## TacVision Debug ## - TacVision_GarbageCollector: TacVision_ETI_Intel: %1", TacVision_ETI_Intel];

		
		_targets = nearestObjects [player, TACVISION_TI_SCANNER_OBJECTLIST, TACVISION_TI_SCANNER_MAXRANGE];
		//diag_log format ["## TacVision Debug ## - _targets: %1", _targets];
		{
			
			if( _x isKindOf "Man" || _x isKindOf "ManStory" || _x isKindOf "ManDiver" || _x isKindOf "ManUrban" || _x isKindOf "ManSniper" || _x isKindOf "ManRecon" || _x isKindOf "ManSupport" && (_x distance player) < TACVISION_TI_SCANNER_INFANTRY_RANGE) then {
				//diag_log format ["## TacVision Debug ## - _x: %1", _x];
				private ["_intel"];
				_type = "Man";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_INFANTRY, TACVISION_MARKER_SIZE_MEDIUMSMALL, _type] call TacVision_TI_ETI;
				_intel = nil;
			};

			if(_x isKindOf "Car" || _x isKindOf "Support" && (_x distance player) < TACVISION_TI_SCANNER_VEHICLE_RANGE) then {
				private ["_intel"];
				_type = "Car";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_VEHICLE, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			if(_x isKindOf "Air" || _x isKindOf "Autonomous" && (_x distance player) < TACVISION_TI_SCANNER_AIR_RANGE) then {
				private ["_intel"];
				_type = "Air";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_AIR, TACVISION_MARKER_SIZE_LARGE, _type] call TacVision_TI_ETI;
				_intel = nil;
			};

			if(_x isKindOf "Armored" && (_x distance player) < TACVISION_TI_SCANNER_ARMORED_RANGE) then {
				private ["_intel"];
				_type = "Armored";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_ARMORED, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			if(_x isKindOf "Ship" && (_x distance player) < TACVISION_TI_SCANNER_SHIP_RANGE) then {
				private ["_intel"];
				_type = "Ship";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_SHIP, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			if(_x isKindOf "SubMarine" && (_x distance player) < TACVISION_TI_SCANNER_SUBMARINE_RANGE) then {
				private ["_intel"];
				_type = "SubMarine";
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_SUBMARINE, TACVISION_MARKER_SIZE_MEDIUM, _type] call TacVision_TI_ETI;
				_intel = nil;
			};
			
			/*
			if(_x isKindOf "Static") then {
				private ["_intel"];
				// call ETI, get info
				_intel = [_x, TACVISION_MARKER_STATIC, TACVISION_MARKER_SIZE_MEDIUMSMALL] call TacVision_TI_ETI;
				_intel = nil;
			};
			*//*
		} forEach _targets;
	};
};
*/

TacVision_getCurrentTime = {
	private ["_date", "_hud", "_ui", "_this"];
	disableSerialization;
	_ui = _this select 0;
	_hud = _this select 1;
	while {true} do {
		_d = date;
		_date = format ["%1:%2", _d select 3, _d select 4];
		_hud ctrlSetText _date;
		_hud ctrlCommit 0;
	};
};


// get current compass heading
TacVision_getCurrentCompassHeading = {
	private ["_dir", "_hud", "_ui", "_this"];
	disableSerialization;
	_ui = _this select 0;
	_hud = _this select 1;
	while {true} do {
		_dir = direction player;
		_hud ctrlSetText str round(_dir);
		_hud ctrlCommit 0;
	};
};


// get current wind direction
TacVision_getCurrentWindDirection = {
	private ["_dir", "_hud", "_ui", "_this"];
	disableSerialization;
	_ui = _this select 0;
	_hud = _this select 1;
	while {true} do {
		_dir = windDir;
		_ret = [_dir] call TacVision_CompassDegreesToString;
		_hud ctrlSetText _ret;
		_hud ctrlCommit 0;
	};
};


// get current wind speed/strength
TacVision_getCurrentWindStrength = {
	private ["_dir", "_hud", "_ui", "_this"];
	disableSerialization;
	_ui = _this select 0;
	_hud = _this select 1;
	while {true} do {
		_dir = windStr;
		//_hud = _ui displayCtrl _hud;
		_hud ctrlSetText str round(_dir);
		_hud ctrlCommit 0;
	};
};


// EOF