/*
	TacVision 0.01a (Tactical Vision)
	
	Provides the user with a Tactical overview/display/hud when wearing tactical glasses/shades.
	
	Author: Spliffz <theSpliffz@gmail.com>
*/
if(isDedicated) exitWith {};

waitUntil { player == player };


#include "config\tacVision_defines.sqf"

if (!TacVision_Enabled) exitWith {};

TacVision_TI_draw3dIcon = compile preprocessFileLineNumbers "TacVision\functions\TV_fnc_draw3dIcon.sqf";
TacVision_TI_ETI = compile preprocessFileLineNumbers "TacVision\functions\TV_fnc_ETI.sqf";
TacVision_TI_MarkIt = compile preprocessFileLineNumbers "TacVision\functions\TV_fnc_markIt.sqf";


// important includes
#include "tacVision_functions.sqf";

waitUntil {!isNull findDisplay 46};


player addAction ["Enable TacVision", { [] execVM "TacVision\tacVision_startup.sqf"; }, [], 1, true, true, "", "(goggles player) == 'G_Tactical_Clear'"];


// dynamic hud stuff
//_draw3D = addMissionEventHandler ["Draw3D", { _this call TacVision_MainLoop; }];




/* wtf.. ??
TacVision_Action_Shades = {
	_mode = _this select 0;
	if(_mode == 1) then {
		private ["_ui", "_shade", "_shade"];
		_ui = uiNamespace getVariable "tacVision_HUD";
		_shade = _ui displayCtrl 17210; // <-- mist een ; ofzo
		_shade ctrlSetFade 0;
		_shade ctrlCommit 0;
		removeAction TacVision_ShadeAction;
		TacVision_ShadeAction = player addAction ["Shades: Off", " [0] call TacVision_Action_Shades; "];
	} else {
		private ["_ui", "_shade", "_shade"];
		_ui = uiNameSpace getVariable "tacVision_HUD";
		_shade = _ui displayCtrl 17210;
		_shade ctrlSetFade 1;
		_shade ctrlCommit 0;
		removeAction TacVision_ShadeAction;
		TacVision_ShadeAction = player addAction ["Shades: Red", " [1] call TacVision_Action_Shades; "];
	};
};

TacVision_ShadeAction = player addAction ["Shades: Red", " [1] call TacVision_Action_Shades; "];
*/








// EOF