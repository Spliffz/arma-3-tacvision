#TacVision 0.1 (Tactical Vision)

Provides the user with a Tactical overview/display/hud when wearing tactical glasses/shades.

Author: Spliffz <theSpliffz@gmail.com>

Features:
- current heading
- wind dir/strength
- GPS Coordinates
- current in-game time
- ATR (Automatic Target Recognition) - The Tactical Glasses & Shades are now able to scan your view for possible threats & targets (WIP, not in release/source)
- MTR (Manual Target Recognition) - With the Tactical Glasses & Shades on you can scan what is directly infront of you, and pass it to the ETI for detailed information.
- ETI (Extended Target Information) - Does what it says, Gives Extended Target/Threat Information


Todo:
Thermal-vision & NVG
Colortint (as in sunglasses)
fix gui scaling for 4:3 monitors
fix ATR!

[Requirements]
Arma 3
Community Base Addons Arma 3 (@CBA_A3)

[Known Issues]
When in-game, wearing Tactical Glasses and using TacVision, Splendid Camera & Config Viewer are crashing upon opening.
I've looked into this, but I've really no idea why. Maybe it has to do with the uiNameSpace, but haven't been able te repro it.
So, if you need those things in-game while you're running TacVision, just take off the glasses first :)


[Development Information]
All my previous attemps for getting the ATR working without a ton of lag have failed.
oneachframe, stacked or not, whileloops with nearobjects.. sigh.. gives me headaches.
Everything is on the repo for those interested.


[Disclaimer]
I (Spliffz) take no responsibility for anything that might happen with your game, computer, life or anything else when using this (my) mod(s).
By acknowledging this, you are now allowed to use this mod and to modify the code, on the condition that you share your changes/the code with me/the community, so that we can all benefit from it.
And if you decide to use this script or parts from it in other scripts/mods or addons then be so kind to put my name in there too.


// EOF