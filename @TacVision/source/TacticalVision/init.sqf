// TacVision (Tactical Vision)
// init.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>


if(isDedicated) exitWith {};

waitUntil {player == player};

// TacVision functions
TacVision_init_startup = compile preprocessFileLineNumbers "\TacticalVision\TacVision\tacVision_startup.sqf";
TacVision_getCurrentWindStrenght = compile preprocessFileLineNumbers "\TacticalVision\TacVision\functions\TV_fnc_getCurrentWindStrength.sqf";
TacVision_getCurrentWindDirection = compile preprocessFileLineNumbers "\TacticalVision\TacVision\functions\TV_fnc_getCurrentWindDirection.sqf";
TacVision_getCurrentCompassHeading = compile preprocessFileLineNumbers "\TacticalVision\TacVision\functions\TV_fnc_getCurrentCompassHeading.sqf";
TacVision_getCurrentGPSGrid = compile preprocessFileLineNumbers "\TacticalVision\TacVision\functions\TV_fnc_getCurrentGPSGridPos.sqf";
TacVision_compassDegreesToString = compile preprocessFileLineNumbers "\TacticalVision\TacVision\functions\TV_fnc_CompassDegreesToString.sqf";
TacVision_getCurrentTime = compile preprocessFileLineNumbers "\TacticalVision\TacVision\functions\TV_fnc_getCurrentTime.sqf";
TacVision_targetFinder = compile preprocessFileLineNumbers "\TacticalVision\TacVision\functions\TV_fnc_cursorTarget.sqf";
TacVision_fnc_ETI = compile preprocessFileLineNumbers "\TacticalVision\TacVision\functions\TV_fnc_ETI.sqf";


// init TacVision!
call compile preprocessFileLineNumbers "\TacticalVision\TacVision\tacVision_init.sqf";


// EOF