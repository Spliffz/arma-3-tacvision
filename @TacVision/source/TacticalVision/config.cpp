// TacVision (Tactical Vision)
// config.cpp
// 2014 - Spliffz <theSpliffz@gmail.com>

class CfgPatches
{
	class TacVision
	{
		units[] = { };
		weapons[] = { };
		requiredAddons[] = {"CBA_Extended_EventHandlers"};
		version = "0.1";
		versionStr = "0.1";
		versionDesc= "Tactical Vision 0.1";
		versionAr[] = {2014,3,30};
		author[] = {"Spliffz"};
	};
};

#include "\TacticalVision\TacVision\tacVision_ctrl_styles.hpp"

class RscTitles 
{
    
	class Default 
	{
		idd = -1;
		fadein = 0;
		fadeout = 0;
		duration = 0;
	};

	#include "\TacticalVision\TacVision\tacVision.hpp"
};

class Extended_PostInit_EventHandlers 
{
	class TacVision
	{
		clientInit = "if(!isDedicated) then { execVM '\TacticalVision\init.sqf'; };";
	};
};

// EOF