
/* #Xygazy
$[
	1.063,
	["TacVision_r6",[[0,0,1,1],0.025,0.04,"GUI_GRID"],0,0,0],
	[2200,"tacv_info_top_right_guiback",[1,"",["0.588124 * safezoneW + safezoneX","-0.00240735 * safezoneH + safezoneY","0.233541 * safezoneW","0.0302222 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1100,"tacV_info_text_currentDirection",[1,"CDir: 138",["0.592812 * safezoneW + safezoneX","0.00500001 * safezoneH + safezoneY","0.0545833 * safezoneW","0.0182963 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[2201,"tacv_info_top_left_guiback",[1,"",["0.199374 * safezoneW + safezoneX","-0.00148143 * safezoneH + safezoneY","0.152707 * safezoneW","0.0859258 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1103,"tacV_info_text_currentTargetInfo",[1,"Target: bla die bla<br/>bladiebla",["0.204011 * safezoneW + safezoneX","0.00314816 * safezoneH + safezoneY","0.144375 * safezoneW","0.077 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1104,"tacV_logo",[1,"TacVision v1.0",["0.927969 * safezoneW + safezoneX","0.973 * safezoneH + safezoneY","0.0670312 * safezoneW","0.022 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1106,"tacV_info_text_windDirection",[1,"CDir: 138",["0.649531 * safezoneW + safezoneX","0.00500001 * safezoneH + safezoneY","0.0545833 * safezoneW","0.0182963 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1107,"tacV_info_text_currentGPS",[1,"124115",["0.70625 * safezoneW + safezoneX","0.00500001 * safezoneH + safezoneY","0.0545833 * safezoneW","0.0182963 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1108,"tacV_info_text_currentTime",[1,"12:43",["0.762969 * safezoneW + safezoneX","0.00500001 * safezoneH + safezoneY","0.0545833 * safezoneW","0.0182963 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]]
]
*/



////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Spliffz, v1.063, #Xygazy)
////////////////////////////////////////////////////////

class tacv_info_top_right_guiback: IGUIBack
{
	idc = 2200;
	x = 0.588124 * safezoneW + safezoneX;
	y = -0.00240735 * safezoneH + safezoneY;
	w = 0.233541 * safezoneW;
	h = 0.0302222 * safezoneH;
};
class tacV_info_text_currentDirection: RscStructuredText
{
	idc = 1100;
	text = "CDir: 138"; //--- ToDo: Localize;
	x = 0.592812 * safezoneW + safezoneX;
	y = 0.00500001 * safezoneH + safezoneY;
	w = 0.0545833 * safezoneW;
	h = 0.0182963 * safezoneH;
};
class tacv_info_top_left_guiback: IGUIBack
{
	idc = 2201;
	x = 0.199374 * safezoneW + safezoneX;
	y = -0.00148143 * safezoneH + safezoneY;
	w = 0.152707 * safezoneW;
	h = 0.0859258 * safezoneH;
};
class tacV_info_text_currentTargetInfo: RscStructuredText
{
	idc = 1103;
	text = "Target: bla die bla<br/>bladiebla"; //--- ToDo: Localize;
	x = 0.204011 * safezoneW + safezoneX;
	y = 0.00314816 * safezoneH + safezoneY;
	w = 0.144375 * safezoneW;
	h = 0.077 * safezoneH;
};
class tacV_logo: RscStructuredText
{
	idc = 1104;
	text = "TacVision v1.0"; //--- ToDo: Localize;
	x = 0.927969 * safezoneW + safezoneX;
	y = 0.973 * safezoneH + safezoneY;
	w = 0.0670312 * safezoneW;
	h = 0.022 * safezoneH;
};
class tacV_info_text_windDirection: RscStructuredText
{
	idc = 1106;
	text = "CDir: 138"; //--- ToDo: Localize;
	x = 0.649531 * safezoneW + safezoneX;
	y = 0.00500001 * safezoneH + safezoneY;
	w = 0.0545833 * safezoneW;
	h = 0.0182963 * safezoneH;
};
class tacV_info_text_currentGPS: RscStructuredText
{
	idc = 1107;
	text = "124115"; //--- ToDo: Localize;
	x = 0.70625 * safezoneW + safezoneX;
	y = 0.00500001 * safezoneH + safezoneY;
	w = 0.0545833 * safezoneW;
	h = 0.0182963 * safezoneH;
};
class tacV_info_text_currentTime: RscStructuredText
{
	idc = 1108;
	text = "12:43"; //--- ToDo: Localize;
	x = 0.762969 * safezoneW + safezoneX;
	y = 0.00500001 * safezoneH + safezoneY;
	w = 0.0545833 * safezoneW;
	h = 0.0182963 * safezoneH;
};
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT END
////////////////////////////////////////////////////////

//--- TacVision_r6
#define IDC_TACVISION_R6_TACV_INFO_TEXT_CURRENTDIRECTION	7524
#define IDC_TACVISION_R6_TACV_INFO_TEXT_CURRENTTARGETINFO	7527
#define IDC_TACVISION_R6_TACV_LOGO				7528
#define IDC_TACVISION_R6_TACV_INFO_TEXT_WINDDIRECTION	7530
#define IDC_TACVISION_R6_TACV_INFO_TEXT_CURRENTGPS		7531
#define IDC_TACVISION_R6_TACV_INFO_TEXT_CURRENTTIME		7532
#define IDC_TACVISION_R6_TACV_INFO_TOP_RIGHT_GUIBACK		8624
#define IDC_TACVISION_R6_TACV_INFO_TOP_LEFT_GUIBACK		8625

