	class tacVision
	{
		
		idd = 17100;
		duration = 1.0e+11; 
		movingEnable = 0;
		enableSimulation = 1;
		name = "tacVision_HUD";
		onLoad = "uiNamespace setVariable ['tacVision_HUD', _this select 0]";
		controlsBackground[] = {};
		objects[] = {};
    
        class Controls 
        {
            class tacv_info_top_right_guiback: IGUIBack
            {
                //idc = 2200;
                idc = 17200;
                x = 0.588124 * safezoneW + safezoneX;
                y = -0.00240735 * safezoneH + safezoneY;
                w = 0.233541 * safezoneW;
                h = 0.0302222 * safezoneH;
            };
            class tacV_info_text_currentDirection: RscStructuredText
            {
                //idc = 1100;
                idc = 17210;
                text = "CDir: 138"; //--- ToDo: Localize;
				sizeEx = 0.03;
                x = 0.592812 * safezoneW + safezoneX;
                y = 0.00500001 * safezoneH + safezoneY;
                w = 0.0545833 * safezoneW;
                h = 0.0182963 * safezoneH;
            };
            class tacv_info_top_left_guiback: IGUIBack
            {
                //idc = 2201;
                idc = 17211;
                x = 0.199374 * safezoneW + safezoneX;
                y = -0.00148143 * safezoneH + safezoneY;
                w = 0.152707 * safezoneW;
                h = 0.0859258 * safezoneH;
            };
            class tacV_info_text_currentTargetInfo: RscStructuredText
            {
                //idc = 1103;
                idc = 17212;
                text = ""; //--- ToDo: Localize;
				sizeEx = 0.03;
                x = 0.204011 * safezoneW + safezoneX;
                y = 0.00314816 * safezoneH + safezoneY;
                w = 0.144375 * safezoneW;
                h = 0.077 * safezoneH;
                lineSpacing = 1; //required for multi-line style
            };
            class tacV_logo: RscStructuredText
            {
                //idc = 1104;
                idc = 17213;
                text = "<t align='left' size='0.8'>TacVision v1.0</t>"; //--- ToDo: Localize;
				sizeEx = 0.01;
                x = 0.927969 * safezoneW + safezoneX;
                y = 0.973 * safezoneH + safezoneY;
                w = 0.0670312 * safezoneW;
                h = 0.022 * safezoneH;
            };
            class tacV_info_text_windDirection: RscStructuredText
            {
                //idc = 1106;
                idc = 17214;
                text = "WDir: 138"; //--- ToDo: Localize;
				sizeEx = 0.03;
                x = 0.649531 * safezoneW + safezoneX;
                y = 0.00500001 * safezoneH + safezoneY;
                w = 0.0545833 * safezoneW;
                h = 0.0182963 * safezoneH;
            };
            class tacV_info_text_currentGPS: RscStructuredText
            {
                //idc = 1107;
                idc = 17215;
                text = "124115"; //--- ToDo: Localize;
				sizeEx = 0.03;
                x = 0.70625 * safezoneW + safezoneX;
                y = 0.00500001 * safezoneH + safezoneY;
                w = 0.0545833 * safezoneW;
                h = 0.0182963 * safezoneH;
            };
            class tacV_info_text_currentTime: RscStructuredText
            {
                //idc = 1108;
                idc = 17216;
                text = "12:43"; //--- ToDo: Localize;
				sizeEx = 0.03;
                x = 0.762969 * safezoneW + safezoneX;
                y = 0.00500001 * safezoneH + safezoneY;
                w = 0.0545833 * safezoneW;
                h = 0.0182963 * safezoneH;
            };

        };
    };
    
    
// EOF