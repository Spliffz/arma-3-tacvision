// TacVision (Tactical Vision)
// fnc_CompassDegreesToString.sqf
// 2014 - Spliffz <theSpliffz@gmail.com>

private ["_degree", "_str"];

//TacVision_CompassDegreesToString = {
_degree = _this select 0;
_str = "";

if(_degree >= 350 && _degree <= 10) then { _str = "> N"; };
if(_degree >= 11 && _degree <= 79) then { _str = "> NE"; };
if(_degree >= 80 && _degree <= 100) then { _str = "> E"; };
if(_degree >= 101 && _degree <= 169) then { _str = "> SE"; };
if(_degree >= 170 && _degree <= 190) then { _str = "> S"; };
if(_degree >= 191 && _degree <= 259) then { _str = "> SW"; };
if(_degree >= 260 && _degree <= 279) then { _str = "> W"; };
if(_degree >= 280 && _degree <= 349) then { _str = "> NW"; };

_str

//};

// EOF